package main

import (
	"fmt"
	"log"

	"github.com/cfabrica46/go-paradigms/go-poo/world"
)

func main() {
	peru := world.NewCountry("Peru", "RENIEC", 10000, 300)

	p1 := world.NewPerson(1, "César", &peru)
	p2 := world.NewPerson(2, "Carlos", &peru)

	err := peru.AddInhabitant(&p1)
	if err != nil {
		log.Fatal(err)
	}

	err = peru.AddInhabitant(&p2)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(peru.GetAllNamesOfInhabitants())

	guitarist := p1.ConvertToGuitarist()
	drummer := p2.ConvertToDrummer()

	band := world.NewBand("los xds")

	err = band.AddGuitarist(&guitarist)
	if err != nil {
		log.Fatal(err)
	}

	err = band.AddDrummer(&drummer)
	if err != nil {
		log.Fatal(err)
	}

	band.Play()

}
