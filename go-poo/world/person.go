package world

type Person struct {
	dni     int
	name    string
	country *Country
}

func NewPerson(dni int, name string, country *Country) Person {
	return Person{dni: dni, name: name, country: country}
}

func (p Person) ConvertToGuitarist() Guitarist {
	return NewGuitarrist(p)
}

func (p Person) ConvertToDrummer() Drummer {
	return NewDrummer(p)
}
