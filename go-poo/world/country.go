package world

type Country struct {
	name          string
	area          int
	population    int
	civilRegistry *CivilRegistry
}

func NewCountry(name, nameCivilRegistry string, area, population int) Country {
	cr := NewCivilRegistry(nameCivilRegistry)
	return Country{name: name, area: area, population: population, civilRegistry: &cr}
}

func (c *Country) AddInhabitant(p *Person) (err error) {
	err = c.civilRegistry.AddInhabitant(p)
	if err != nil {
		return
	}
	return
}

func (c Country) GetAllNamesOfInhabitants() (names []string) {
	for i := range c.civilRegistry.inhabitants {
		names = append(names, c.civilRegistry.inhabitants[i].name)
	}
	return
}
