package world

import (
	"errors"
	"fmt"
)

type Band struct {
	name       string
	guitarists []*Guitarist
	drummer    *Drummer
}

func NewBand(name string) Band {
	return Band{name: name}
}

func (b *Band) AddGuitarist(g *Guitarist) (err error) {
	if len(b.guitarists) == 2 {
		err = errors.New("Complete Guitarists")
		return
	}

	b.guitarists = append(b.guitarists, g)
	g.band = b
	return
}

func (b *Band) AddDrummer(d *Drummer) (err error) {
	if b.drummer != nil {
		err = errors.New("Complete Drummer")
		return
	}

	b.drummer = d
	d.band = b
	return
}

func (b Band) Play() {
	fmt.Printf("%s plays their instruments\n", b.name)
	for i := range b.guitarists {
		fmt.Printf("%s plays the guitar\n", b.guitarists[i].name)
		b.guitarists[i].PlayInstrument()
	}

	fmt.Printf("%s plays the drums\n", b.drummer.name)
	b.drummer.PlayInstrument()
}
