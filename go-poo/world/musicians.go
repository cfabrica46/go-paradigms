package world

import (
	"errors"
	"fmt"
)

type Musician interface {
	PlayInstrument()
	GetBand()
}

type Guitarist struct {
	Person
	band *Band
}

func NewGuitarrist(p Person) Guitarist {
	return Guitarist{Person: p}
}

func (g Guitarist) GetBand() (band Band, err error) {
	if g.band == nil {
		err = errors.New("Is not in a band")
		return
	}

	band = *g.band
	return
}

func (g Guitarist) PlayInstrument() {
	fmt.Println("tiritiritiritri >:v")
}

type Drummer struct {
	Person
	band *Band
}

func NewDrummer(p Person) Drummer {
	return Drummer{Person: p}
}

func (d Drummer) GetBand() (band Band, err error) {
	if d.band == nil {
		err = errors.New("Is not in a band")
		return
	}

	band = *d.band
	return
}

func (d Drummer) PlayInstrument() {
	fmt.Println("badabum tsss >:v")
}
