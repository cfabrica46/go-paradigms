package world

import (
	"errors"
)

type CivilRegistry struct {
	name        string
	inhabitants map[int]*Person
}

func NewCivilRegistry(name string) CivilRegistry {
	return CivilRegistry{name: name, inhabitants: make(map[int]*Person)}
}

func (c *CivilRegistry) AddInhabitant(p *Person) (err error) {
	if c.inhabitants[p.dni] != nil {
		err = errors.New("DNI is already registered")
		return
	}
	c.inhabitants[p.dni] = p
	return
}
