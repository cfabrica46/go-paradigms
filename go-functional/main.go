package main

import "fmt"

func main() {
	firstClass()
	closure()
	recursive()
}

//First-Class and higher-order functions
func firstClass() {
	fmt.Println("First-Class function")
	var users = []string{"Cesar", "Arturo", "Carlos", "Luis"}

	var out = mapForEach(users, func(v string) int {
		return len(v)
	})
	fmt.Println(out)
	fmt.Println()
}

func mapForEach(arr []string, fn func(v string) int) []int {
	var newArray = []int{}
	for _, v := range arr {
		newArray = append(newArray, fn(v))
	}
	return newArray
}

//Closures
func closure() {
	fmt.Println("Closure")
	var add10 = add(10)
	var add20 = add(20)
	var add30 = add(30)

	fmt.Println(add10(5))
	fmt.Println(add20(5))
	fmt.Println(add30(5))
	fmt.Println()
}

func add(x int) func(y int) int {
	return func(y int) int {
		return x + y
	}
}

//Recursive
func recursive() {
	fmt.Println("Recursive")
	fmt.Println(factorial(5))
	fmt.Println()
}

func factorial(num int) int {
	result := 1
	for ; num > 0; num-- {
		result *= num
	}
	return result
}
